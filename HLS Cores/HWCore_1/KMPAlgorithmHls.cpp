#define AP_INT_MAX_W 4096
#include <ap_int.h>
#include <hls_stream.h>

# define M 100
# define N 35000
# define K 10

#define D_TYPE int

#define AP_INT_MAX_W 4096

template<int D> struct ap_axis{
    ap_int<D> data;
    ap_uint<1> last;
  };

template struct my_ap_axis_struct{
    D_TYPE data;
    bool last;
  };

typedef ap_axis<32> AXI_VAL;

typedef struct my_ap_axis_struct MY_TYPE_VAL;

typedef hls::stream<AXI_VAL> AXI_STREAM_VAL;

MY_TYPE_VAL operator + (const MY_TYPE_VAL& lhs, const MY_TYPE_VAL &rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data + rhs.data;
	return temp;
}

MY_TYPE_VAL operator - (const MY_TYPE_VAL& lhs, const MY_TYPE_VAL &rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data - rhs.data;
	return temp;
}

MY_TYPE_VAL operator * (const D_TYPE& lhs, const MY_TYPE_VAL &rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs * rhs.data;
	return temp;
}

MY_TYPE_VAL operator * (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data * rhs;
	return temp;
}

MY_TYPE_VAL operator * (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data * rhs.data;
	return temp;
}

MY_TYPE_VAL operator / (const D_TYPE& lhs, const MY_TYPE_VAL &rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs / rhs.data;
	return temp;
}

MY_TYPE_VAL operator / (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data / rhs;
	return temp;
}

MY_TYPE_VAL operator / (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	MY_TYPE_VAL temp;
	temp.data = lhs.data / rhs.data;
	return temp;
}

bool operator == (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	return lhs.data == rhs.data;
}

bool operator == (const D_TYPE &lhs, const MY_TYPE_VAL& rhs) {
	return lhs == rhs.data;
}

bool operator == (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data == rhs;
}

bool operator < (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	return lhs.data < rhs.data;
}

bool operator <= (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	return lhs.data <= rhs.data;
}

bool operator > (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	return lhs.data > rhs.data;
}

bool operator >= (const MY_TYPE_VAL &lhs, const MY_TYPE_VAL& rhs) {
	return lhs.data >= rhs.data;
}

bool operator > (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data > rhs;
}

bool operator >= (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data >= rhs;
}

bool operator > (const D_TYPE& lhs, const MY_TYPE_VAL &rhs ) {
	return lhs > rhs.data;
}

bool operator >= (const D_TYPE& lhs, const MY_TYPE_VAL &rhs ) {
	return lhs >= rhs.data;
}

bool operator < (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data < rhs;
}

bool operator <= (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data <= rhs;
}

bool operator < (const D_TYPE& lhs, const MY_TYPE_VAL &rhs ) {
	return lhs < rhs.data;
}

bool operator <= (const D_TYPE& lhs, const MY_TYPE_VAL &rhs ) {
	return lhs <= rhs.data;
}

D_TYPE operator - (const D_TYPE &lhs, const MY_TYPE_VAL& rhs) {
	return lhs - rhs.data;
}

D_TYPE operator - (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data - rhs;
}

D_TYPE operator + (const D_TYPE &lhs, const MY_TYPE_VAL& rhs) {
	return lhs + rhs.data;
}

D_TYPE operator + (const MY_TYPE_VAL &lhs, const D_TYPE& rhs) {
	return lhs.data + rhs;
}

D_TYPE operator % (const MY_TYPE_VAL &lhs, const D_TYPE &rhs) {
	return lhs.data % rhs;
}

void operator >> (AXI_STREAM_VAL& edge, MY_TYPE_VAL &data) {
	AXI_VAL temp;
	union
	{
		int ival;
		D_TYPE oval;
	} converter;

	edge >> temp;
	converter.ival = temp.data;
	data.data = converter.oval;
}

void operator << (AXI_STREAM_VAL& edge, MY_TYPE_VAL data) {
	AXI_VAL temp;
	union
	{
		int oval;
		D_TYPE ival;
	} converter;

	converter.ival = data.data;
	temp.data = converter.oval;
	temp.last = data.last;
	edge << temp;
}

int Max( int a, int b ){
	if(a > b)
		return a;
	else
		return b;
}

void FunzionePrefisso( char *S, int n, int pref[] ){

    int i;
    int h = 0, l, r;

    pref[ 0 ] = n;

    while( S[ h ] == S[ 1 + h ])
        h++;

    pref[ 1 ] = h;
    l = 1;
    r = 1;

    for( i = 2; i < n ; i++){
        if( r < i ){
            h = 0;

            while( S[ h ] == S[ i + h ] )
                h++;

            pref[ i ] = h;
            l = i;
            r = i + h - 1;
        }

        else if( pref[ i - l ] < r - i + 1)
            pref[ i ] = pref[ i - l ];

        else {
            h = r - i + 1;

            while( S[ h ] == S[ i + h ] )
                h++;

            pref[ i ] = h;
            l = i;
            r = i + h - 1;
        }
    }
}

void KnuthMorrisPratt( AXI_STREAM_VAL &DMA_In, AXI_STREAM_VAL &DMA_Out ){
	
	MY_TYPE_VAL intData;
	MY_TYPE_VAL n, m;
	
	DMA_In >> m;
	DMA_In >> n;


	//m (peptide) e n (proteina) sono la lunghezza del testo con sentinella

	char Pchar[ M ];
	char Tchar[ N ];
	int pref[ M ];
	/*d e' lo spostamento che il pattern deve effettuare
	  quando si e' trovato il primo mismatch */
	int d[ M ];

	MY_TYPE_VAL res;
	int i, j, h, numCharIt;

	int mask = 0xFF;

	int numOfInt = m.data / 4;
	int numOfRemainingChars = m % 4;

	Li0:for( i = 0; i <= numOfInt; i++ )
	{
		if(i == numOfInt && numOfRemainingChars == 0)
			break;

		DMA_In >> intData;

		if(i == numOfInt)
			numCharIt = numOfRemainingChars;
		else
			numCharIt = 4;

		for(j = 0; j < numCharIt; ++j)
			Pchar[i*4+j] = (intData.data >> (8*j)) & mask; 
	}

	numOfInt = n.data / 4;
	numOfRemainingChars = n % 4;

	Li1:for( i = 0; i <= numOfInt; i++ )
	{
		if(i == numOfInt && numOfRemainingChars == 0)
			break;

		DMA_In >> intData;

		if(i == numOfInt)
			numCharIt = numOfRemainingChars;
		else
			numCharIt = 4;

		for(j = 0; j < numCharIt; ++j)
			Tchar[i*4+j] = (intData.data >> (8*j)) & mask; 
	}

	//chiamo la funzione prefisso

	FunzionePrefisso( Pchar, m.data, &pref[0]);

	Lj0:for( j = 0; j < m; j++)
		d[ j ] = j + 1;

	Lh0:for( h = m - 1; h >= 0; h--)
		d[ h + pref [ h ] ] = h;

	i = 0;
	j = 0;

	Li2:while( i < n - m + 1 ){

		Lj1:while( Pchar[ j ] == Tchar[ i + j ] )
           		j++;

        	if( j > m.data - 2 ){ //Aggiunto anche qui
        		res.data = i + 1;
        		res.last = 0;
        		DMA_Out << res;
        		}

        	i = i + d[ j ];

        	j = Max( 0, j - d[ j ] );

    	}

	res.data = -1;
	res.last = 1;
	DMA_Out << res;
}
