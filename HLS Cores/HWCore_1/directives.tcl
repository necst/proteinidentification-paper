############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2013 Xilinx Inc. All rights reserved.
############################################################

set_directive_interface -mode ap_ctrl_none -register -latency 0 "KnuthMorrisPratt"

set_directive_resource -core AXI4Stream -metadata {-bus_bundle DMA_Out} "KnuthMorrisPratt" DMA_Out
set_directive_resource -core AXI4Stream -metadata {-bus_bundle DMA_In} "KnuthMorrisPratt" DMA_In

#set_directive_dataflow corr
# set_directive_pipeline dac1/Li0
# set_directive_pipeline dac1/Li1
# set_directive_pipeline dac1/Li2
# set_directive_pipeline dac1/Li3

# set_directive_pipeline dac1/Lj0
# set_directive_pipeline dac1/Lk0
#set_directive_pipeline corr/Lk1

#set_directive_pipeline corr/Lk2

#set_directive_pipeline jacobi_0/Lj0
#set_directive_pipeline jacobi_0/Lj1
#set_directive_unroll -skip_exit_check -factor 20 jacobi_0/Lj0
#set_directive_unroll -factor 20 jacobi_0/Lj1
