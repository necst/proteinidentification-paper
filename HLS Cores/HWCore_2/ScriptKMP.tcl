############################################################
## This file is generated automatically by Vivado HLS.
## Please DO NOT edit it.
## Copyright (C) 2015 Xilinx Inc. All rights reserved.
############################################################
open_project KMP_v3
set_top KnuthMorrisPratt
add_files ./KMPAlgorithmHls_ProteinaIntera.cpp
open_solution "solution1"
set_part {xc7z020clg484-1}
create_clock -period 10 -name default
source "./DirettiveKMP_In_Out_stream.tcl"
#csim_design
csynth_design
#cosim_design
export_design -format ip_catalog
