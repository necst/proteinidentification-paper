/*
 * Copyright (c) 2015 Politecnico di Milano
 *
 * Authors: Gea Bianchi         gea.bianchi@mail.polimi.it
 *          Fabiola Casasopra   fabiola.casasopra@mail.polimi.it
 *          Gianluca Durelli    gianlucacarlo.durelli@polimi.it
 */


#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <unistd.h>
#include <inttypes.h>
#include <sys/time.h>
#include <stdio.h>

#include <iostream>
#include <vector>
#include <map>

#define N 35000
#define M 100
#define ID 7
#define LUNG_NOME 100
#define TEMP 100
#define POSIX 10

typedef struct {
    char UniqueIdentifier[ ID ];
    char EntryName[ LUNG_NOME ];
    char ProteinName[ LUNG_NOME ];
    char Sequence[ N ];
} Proteina;

typedef struct{
    std::string nomeFunzione;
    uint64_t tempo;
    int lengthPeptide;
    int lengthProteina;
} Intervallo;

std::vector<Proteina> database;
std::vector<Intervallo> intervalli;

uint64_t getTime(){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    return tv.tv_sec * 1000 * 1000 + tv.tv_usec;
}

/*
 *Max calcola il massimo tra due interi
 */

int Max( int, int);

/*
 *Funzione Prefisso: preprocessig per l'algoritmo KMP
 */

void FunzionePrefisso( char *, int, int []);

/*
 *Knuth Morris Pratt:
 *Riceve in ingresso la stringa in cui effettuare la ricerca,
 *la sottostringa da ricercare, e le lunghezze delle due stringhe,
 *rispettivamnte della stringa e poi della sottostringa.
 *Infine, riceve il puntatore al vettore in cui salva le posizioni trovate
 */

void KnuthMorrisPratt( char *, char *, int, int, int []);

/*
 *AggiustaFile
 *Aggiunge in fondo al file la stringa ">EndOfFile".
 *Riceve in ingresso il file da modificare.
 */

void AggiustaFile( FILE * );

/*
 *LetturaProteina
 *Legge dal file una proteina alla volta inizializza i campi della
 *struttura dato proteina.
 *Riceve in ingresso il file contenente le poteine, la proteina da
 *inizializzare, un intero che mi indica se sto analizzando la prima
 *proteina, una stringa che mi serve per sapere quando arrivo alla
 *fine del file.
 */

void LetturaProteina( FILE *, Proteina *, int, char * );

/*
 *LetturaUniqueIdentifier
 *Assegno a Proteina lo UniqueIdentifier
 *Riceve in ingresso la stringa in cui salvare lo UniqueIdentifier
 *e la stringa letta dal file in cui e' contenuto. (?)
 */

void LetturaUniqueIdentifier( char *, char * );

/*
 *LetturaEntryName
 *Assegno a Proteina l'EntryName
 *Riceve in ingresso la stringa in cui salvare l'EntryName
 *e la stringa letta dal file in cui e' contenuto. (?)
 */

void LetturaEntryName( char *, char * );

/*
 *LetturaProteinName
 *Leggo dal file il nome della proteina e assegno ProteinName
 *a Proteina
 *Riceve in ingresso il file delle proteine e la stringa
 *in cui salvare ProteinName
 */

void LetturaProteinName( FILE *, char * );

/*
 *LetturaSequence
 *Leggo dal file la sequenza della proteina e assegno Sequence
 *a Proteina
 *Riceve in ingresso il file delle proteine, la stringa in cui
 *salvare Sequence e una stringa che mi serve per capire se sono
 *arrivato in fondo al file
 */

void LetturaSequence( FILE *, char *, char * );

int main(int argc, char **argv){

    if(argc!=2){
        printf("Usage: ./arm NUM_PROTEINS\n");
        exit(0);
    }


    FILE *fPept, *fProt, *fRis = NULL, *fProtTrovate = NULL, *fPosix = NULL;
    char Peptide[ M ] = " ", VecchioPeptide[ M ], Temp[ N ], Tmp[ TEMP ];
    Proteina proteinaInCuiCercare;
    
    std::vector<std::string> proteinaSconosciuta;
    std::map<std::string, std::map<std::string, std::vector<int>>> risultatoRicerca;
    
    Proteina proteina;
    Intervallo intervallo;
    
    int posix[ POSIX ];
    int h, i, j;
    int DimensioneMassima = 0;
    int ProteinaTrovata = 0;
    int PrimaProteina = 1;
    int PrimoPeptide = 1;
    uint64_t start, end, tot = 0;
    std::string IdTrovato;
    
    uint64_t tw1_start, tw1_end_w2_start, tw2_end_for_start, tfor_end;
    uint64_t tforint_start, tforint_end;
    uint64_t tAlgo_start, tAlgo_end;
    
    fPept = fopen( "KI67_HUMAN.txt", "r" );
    fProt = fopen( "uniprot_human_FASTA_canonical.txt", "r+" );
    
    if( fPept == NULL && fProt == NULL ){
        printf( "\nERRORE! Non posso aprire i file\n\n" );
        return 0;
    }
    
    AggiustaFile( fProt );
    
    tw1_start = getTime();
    
    do{
        //leggo la massa del peptide
        fscanf( fPept, "%s", Tmp );
        
        //se non sono alla fine del file
        if( strncmp( ">EndOfFile", Tmp, 10 ) ){
            
            //leggo il nuovo peptide
            fscanf( fPept, "%s", Peptide );
            
            //aggiungo la sentinella
            strcat( Peptide, "x" );
            
            std::string strPeptide(Peptide);
            proteinaSconosciuta.push_back(strPeptide);
            
        }
        
    }while( strncmp( ">EndOfFile", Tmp, 10 ) );
    
    tw1_end_w2_start = getTime();
    
    do{
        LetturaProteina( fProt, &proteina, PrimaProteina, Temp );
        
        strcat( proteina.Sequence, "z" );
        
        if( PrimaProteina )
            PrimaProteina = 0;
        
        database.push_back(proteina);
        
    }while( strncmp( ">EndOfFile", Temp, 10 ) );
    
    tw2_end_for_start = getTime();
    
    /*
     * Algoritmo Naive
     */
    
    PrimaProteina = 1;
    
    int numProteins = atoi(argv[1]);

    numProteins = numProteins < database.size() ? numProteins : database.size();
    
    /*
     * Algoritmo KMP
     */
    
    tw2_end_for_start = getTime();
    
    PrimaProteina = 1;
    
    for( j = 0; j < numProteins; j++ ){
        
        std::map<std::string, std::vector<int>> result;
        
        tforint_start = getTime();
        
        for( i = 0; i < proteinaSconosciuta.size(); i++ ){
            
            if( i == 0 || strcmp( proteinaSconosciuta[ i ].c_str(), proteinaSconosciuta[ i - 1 ].c_str() ) ){
                
                proteinaInCuiCercare = database[j];
                
                strcpy(Peptide, proteinaSconosciuta[i].c_str());
                
                tAlgo_start = getTime();
                
                KnuthMorrisPratt( Peptide, proteinaInCuiCercare.Sequence, strlen( proteinaInCuiCercare.Sequence ), proteinaSconosciuta[i].size(), posix );
                
                int k = 0;
                while ( k < POSIX && posix[ k ]!= -1) {
                    result[ proteinaSconosciuta[ i ] ].push_back( posix[ k ] );
                    k++;
                    
                }
                
                tAlgo_end = getTime();
                    
                intervallo.nomeFunzione = "KMP";
                intervallo.tempo = tAlgo_end - tAlgo_start;
                intervallo.lengthPeptide = strlen( Peptide ) - 1;
                intervallo.lengthProteina = strlen( proteinaInCuiCercare.Sequence ) - 1;
                if( j == 0 )
                    intervalli.push_back( intervallo );
                    
            }
        }
        
        tforint_end = getTime();
        
        intervallo.nomeFunzione = "TuttiPeptidiKMP";
        intervallo.tempo = tforint_end - tforint_start;
        intervallo.lengthPeptide = 0;
        intervallo.lengthProteina = strlen( proteinaInCuiCercare.Sequence ) - 1;
        intervalli.push_back( intervallo );
        
        if (result.size() != 0) {
            risultatoRicerca[proteinaInCuiCercare.UniqueIdentifier] = result;
        }
    
        PrimaProteina = 0;
    
    }
    
    tfor_end = getTime();
    
    intervallo.nomeFunzione = "TutteProteineKMP";
    intervallo.tempo = tfor_end - tw2_end_for_start;
    intervallo.lengthPeptide = 0;
    intervallo.lengthProteina = 0;
    intervalli.push_back( intervallo );
    
    //funzione ricerca proteina "giusta"
    
    for (auto &h : risultatoRicerca){
        
        if( h.second.size() > DimensioneMassima ){
            DimensioneMassima = h.second.size();
            IdTrovato = h.first;
        }
    }
    
    intervallo.nomeFunzione = "LetturaPeptide";
    intervallo.tempo = tw1_end_w2_start - tw1_start;
    intervallo.lengthPeptide = 0;
    intervallo.lengthProteina = 0;
    intervalli.push_back( intervallo );
    
    intervallo.nomeFunzione = "LetturaProteina";
    intervallo.tempo = tw2_end_for_start - tw1_end_w2_start;
    intervallo.lengthPeptide = 0;
    intervallo.lengthProteina = 0;
    intervalli.push_back( intervallo );
    
    //std::cout << IdTrovato << "\t" << DimensioneMassima;
    //std::cout << std::endl;
    
    for( i = 0; i < intervalli.size(); i++ ){
        std::cout << "ARM" << "\t" << intervalli[i].nomeFunzione << "\t" << intervalli[i].tempo << "\t";
        std::cout <<intervalli[i].lengthProteina << "\t";
        std::cout <<intervalli[i].lengthPeptide;
        std::cout << std::endl;
    }
    
    return 0;
}

/*
 * ########## FUNZIONI ##########
 */

//funzione di stampa tutto

//    for (auto &h : risultatoRicerca){
//        std::cout << h.first << "\n";
//
//        for(auto &i : h.second){
//            std::cout << "\t" << i.first << "\t";
//
//            for(j = 0; j < i.second.size(); j++)
//                std::cout << i.second[j] << " ";
//
//            std::cout << std::endl;
//        }
//
//        std::cout << std::endl;
//    }


int Max( int a, int b ){
    
    if( a > b )
        return a;
    else
        return b;
    
}

void FunzionePrefisso( char *S, int n, int pref[] ){
    
    int i;
    int h = 0, l, r;
    
    pref[ 0 ] = n;
    
    while( S[ h ] == S[ 1 + h ])
        h++;
    
    pref[ 1 ] = h;
    l = 1;
    r = 1;
    
    for( i = 2; i < n ; i++){
        if( r < i ){
            h = 0;
            
            while( S[ h ] == S[ i + h ] )
                h++;
            
            pref[ i ] = h;
            l = i;
            r = i + h - 1;
        }
        
        else if( pref[ i - l ] < r - i + 1)
            pref[ i ] = pref[ i - l ];
        
        else {
            h = r - i + 1;
            
            while( S[ h ] == S[ i + h ] )
                h++;
            
            pref[ i ] = h;
            l = i;
            r = i + h - 1;
        }
    }
}

void KnuthMorrisPratt( char *P, char *T, int n, int m , int output[]){
    
    //N.B.: le stringe devono gia' avere la sentinella
    //NB m, n linghezze senza sentinella
    
    int pref[ M ];
    int h, i, j, occ = 0;
    /*d e' lo spostamento che il pattern deve effettuare
     quando si e' trovato il primo mismatch */
    int d[ M ];
    
    //uso m + 1 poiché devo tener conto anche del prefisso
    //m = strlen( P ) - 1
    
    FunzionePrefisso( P, m + 1, &pref[ 0 ] );
    
    /*costruisco il vettore che mi permette di "saltare"
     devo usare d[ j ] = j + 1 poiché parto da j = 0
     e per lo stesso motivo j = h + pref( h )*/
    
    for( j = 0; j < m + 1; j++ )
    /*diamo a ogni d[ j ] il valore j + 1 che e' il
     massimo dei valori tra cui minimizzare*/
        d[ j ] = j + 1;
    
    for( h = m ; h >= 0; h-- )
    /*aggiorno d[ j ] quando trovo un h t.c.
     j = h + pref( h ) */
        d[ h + pref[ h ] ] = h;
    
    i=0;
    j=0;
    
    while( i < n - m + 1){
        while( P[ j ] == T[ i + j ] ){
            j++;
        }
        
        //devo considerare m - 1 sempre perché j parte da 0
        
        if( j > m - 1 ){
            output[ occ ] = i + 1;
            occ++;
        }
        
        i = i + d[ j ];
        
        //in MAX devo mettere 0 perché j deve partire da 0
        j = Max( 0, j - d[ j ] );
        
    }
    
    output[ occ ] = -1;
    
}


void AggiustaFile( FILE *f ){
    
    char Temp[ TEMP ];
    
    fseek( f, -10, 2 );
    
    if(strncmp( Temp, ">End", 4 )){
        fseek( f, 0, 2 );
        fputs( "\n>EndOfFile", f );
    }
    
    rewind( f );
    
}

void LetturaProteina( FILE *f , Proteina *proteina, int PrimaProteina, char *t ){
    
    char Temp[ N ];
    int i;
    
    if( PrimaProteina ){
        rewind( f );
        fscanf( f, "%s", t );
    }
    
    LetturaUniqueIdentifier( proteina -> UniqueIdentifier, t );
    
    LetturaEntryName( proteina -> EntryName, t );
    
    LetturaProteinName( f, proteina -> ProteinName);
    
    LetturaSequence( f, proteina -> Sequence, Temp );
    
    for( i = 0; i < strlen( Temp ); i++)
        t[ i ] = Temp[ i ];
    
    t[ strlen( Temp ) ] = '\0';
    
}

void LetturaUniqueIdentifier( char *UniqueIdentifier, char *t ){
    
    int i;
    
    for( i = 4; i < 10; i++ )
        UniqueIdentifier[ i - 4 ] = t[ i ];
}

void LetturaEntryName( char *EntryName, char *Temp ){
    
    int i;
    
    for( i = 11; i < strlen( Temp ); i++ )
        EntryName[ i - 11 ] = Temp[ i ];
    
    EntryName[ strlen( Temp ) - 11 ] = '\0';
}

void LetturaProteinName( FILE *f, char *ProteinName ){
    
    int i;
    char Temp[ TEMP ];
    
    fscanf( f, "%s", ProteinName );
    
    do{
        
        fscanf( f, "%s", Temp );
        
        if( strncmp( Temp, "OS=", 3 ) ){
            strcat( ProteinName, " " );
            strcat( ProteinName, Temp );
        }
        
    }while( strncmp( Temp, "OS=", 3 ) );
}

void LetturaSequence( FILE *f, char *Sequence, char *t){
    
    int i;
    char Temp[ N ];
    
    do{
        
        fscanf( f, "%s", Temp );
        
    }while( strncmp( Temp, "SV=", 3 ) );
    
    fscanf( f, "%s", Sequence );
    
    do{
        
        fscanf( f, "%s", Temp );
        
        if( strncmp( Temp, ">", 1 ) )
            strcat( Sequence, Temp );
        
    }while( strncmp( Temp, ">", 1 ) );
    
    for( i = 0; i < strlen( Temp ); i++)
        t[ i ] = Temp[ i ];
    
    t [ strlen( Temp ) ] = '\0';
    
}
