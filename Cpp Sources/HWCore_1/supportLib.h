/*
 * Copyright (c) 2015 Politecnico di Milano
 *
 * Authors: Gianluca Durelli  	gianlucacarlo.durelli@polimi.it
 */


#ifndef SUPPORTLIB_H_
#define SUPPORTLIB_H_

#define DMA_LEN 1ULL * 1024

unsigned long writeDMA(int fd, unsigned char *buffer, unsigned long length);
unsigned long readDMA(int fd, unsigned char *buffer, unsigned long length);
unsigned long readDMA_simple(int fd, unsigned char *buffer, unsigned long length);
unsigned long reconfigure(char *bitstream);
unsigned long getTime();

#endif /* SUPPORTLIB_H_ */
