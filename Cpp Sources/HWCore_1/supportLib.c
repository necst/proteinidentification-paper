/*
 * Copyright (c) 2015 Politecnico di Milano
 *
 * Authors: Gianluca Durelli  	gianlucacarlo.durelli@polimi.it
 */


#include "supportLib.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>

unsigned long getTime(){
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec * 1000000 + tv.tv_usec;
}

unsigned long reconfigure(char *bitstream){
	char buf[1024];

	unsigned long start, end;

	start = getTime();
	system("echo 1 > /sys/devices/amba.0/f8007000.ps7-dev-cfg/is_partial_bitstream");

	sprintf(buf, "cat %s > /dev/xdevcfg", bitstream);
	printf("Issuing reconfiguration of %s\n", bitstream);
	system(buf);
	end = getTime();

	return end-start;
}

unsigned long readDMA(int fd, unsigned char *buffer, unsigned long length){
	unsigned long byteMoved = 0;
	unsigned long byteToTransfer = 0;
	unsigned long start, end;

	start = getTime();
	while(byteMoved != length){
		byteToTransfer = length - byteMoved > DMA_LEN ? DMA_LEN : length - byteMoved;
		byteMoved += read(fd, &buffer[byteMoved], byteToTransfer);
	}
	end = getTime();

	return end - start;
}

unsigned long readDMA_simple(int fd, unsigned char *buffer, unsigned long length){
	unsigned long start, end;
	
	start = getTime();

	read(fd, buffer, length);

	end = getTime();
	return end - start;
}

unsigned long writeDMA(int fd, unsigned char *buffer, unsigned long length){
	unsigned long byteMoved = 0;
	unsigned long byteToTransfer = 0;
	unsigned long start, end;

	start = getTime();

	while(byteMoved != length){
		byteToTransfer = length - byteMoved > DMA_LEN ? DMA_LEN : length - byteMoved;
		byteMoved += write(fd, &buffer[byteMoved], byteToTransfer);
	}

	end = getTime();

	return end-start;
}
