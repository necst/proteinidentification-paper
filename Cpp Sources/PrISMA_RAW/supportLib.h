/*
 * supportLib.h
 *
 *  Created on: Jan 24, 2015
 *      Author: gianluca
 */

#ifndef SUPPORTLIB_H_
#define SUPPORTLIB_H_

#define DMA_LEN 1ULL * 1024
#define SYNCH_MODE	0x01
#define ASYNCH_MODE	0x02
#define INQUIRE_BUSY	0x04
#define FINISH_TRANSFER	0x08

unsigned long writeDMA(int fd, unsigned char *buffer, unsigned long length);
unsigned long readDMA(int fd, unsigned char *buffer, unsigned long length);
unsigned long readDMA_simple(int fd, unsigned char *buffer, unsigned long length);
unsigned long readDMA_asynch(int fd, unsigned char *buffer, unsigned long length);
unsigned long reconfigure(char *bitstream);
unsigned long getTime();

#endif /* SUPPORTLIB_H_ */
