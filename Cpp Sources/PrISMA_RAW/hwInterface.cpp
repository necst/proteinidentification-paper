#include "interface.hpp"
#include "supportLib.h"

#include <fcntl.h>
#include <assert.h>
#include <unistd.h>

#include <iostream>

#include <string.h>

#define MAX_MATCHES 512

typedef struct{
	int protein;
	int position;
} Match;

//Send command
void sendCommand(int fd, int cmd){
	writeDMA(fd, (unsigned char *) &cmd, sizeof(int));
}

//Sending database
void sendDatabase(int fd, std::pair<int, std::string> db){

	int i;
	unsigned int dbLength = db.first;

    // std::cout << "Sending database length and database\t" << dbLength << std::endl;

	// Send protein length
	writeDMA(fd, (unsigned char *) &dbLength, sizeof(int));

	// Send protein sequence
	writeDMA(fd, (unsigned char *) db.second.c_str(), db.second.size() * sizeof(char));
}

// Sending the number of valid peptides
void sendNumPept(int fd, int num){
    // std::cout << "\n Sending num of peptides = "<< num <<std::endl;
	writeDMA(fd, (unsigned char *) &num, sizeof(int));
}

// Sending peptides
void sendPeptide(int fd, std::pair<int, std::string> peptide){
   	int peptLength = peptide.first;
	int i;

	// std::cout << peptide.second << "\t" << peptLength << std::endl;
	writeDMA(fd,(unsigned char *) &peptLength, sizeof(int));
	writeDMA(fd, (unsigned char *) peptide.second.c_str(), peptide.second.size() * sizeof(char));

}

//Check results
unsigned long checkResults(int fd){
    // int numProtein, position, i;
    int numProtein, position, i;

    unsigned long matches = 0;

    do{
        // std::cout << "\nReading..." <<std::endl;
        readDMA(fd, (unsigned char *) &numProtein, sizeof(int));

		// if(numProtein == -1)
		// 	std::cout << "No match found! Num = " << numProtein << std::endl;

		if(numProtein!= -1){
			readDMA(fd, (unsigned char *) &position, sizeof(int));

			matches++;
			// std::cout << "Save the position of the match " << position << " in the protein ";
			// std::cout << numProtein << " in dma " << fd << std::endl;
		}
    }while(numProtein!=-1);

    return matches;
}


//Check results
unsigned long checkResultsBuffer(int fd){
    // int numProtein, position, i;
    int numProtein, position, i;

    Match matches[MAX_MATCHES];
    unsigned long dataRead;
    unsigned long curMatches;

    unsigned long numMatches = 0;

    int foundEnd = 0;

    do{
    	foundEnd = 0;

    	// std::cout << "Attemping to read " << sizeof(Match) * MAX_MATCHES << std::endl;
        // std::cout << "\nReading..." <<std::endl;
        dataRead = readDMA_simple(fd, (unsigned char *) matches, sizeof(Match) * MAX_MATCHES);

        curMatches = dataRead / sizeof(Match);

        // std::cout << "Read Data " << dataRead << std::endl;
        // std::cout << "Matches " << curMatches << std::endl;

        // matches += curMatches;

        for(i=0; i<curMatches; i++){
        	// std::cout << matches[i].protein << "\t" << matches[i].position << std::endl;
        	if(matches[i].protein==-1){
        	    foundEnd=1;
        	    break;
        	}
        	numMatches++;
        }


    }while(foundEnd == 0);

    return numMatches;
}


void openDMAs(std::vector<int> & dmas, int NumDMA){
	for(int i = 0; i<NumDMA; i++){
		std::string devName = "/dev/axi-dma" + std::to_string(i+1);
		// std::cout << "Opening " << devName << devName << std::endl;
		int fd = open(devName.c_str(), O_RDWR);
		assert(fd>0);
		dmas.push_back(fd);
	}
}

void closeDMAs(std::vector<int> & dmas){
	for(auto dma : dmas)
		close(dma);
}

// unsigned long KMPTest(){


// 	char *db = (char*)malloc(sizeof(char) * 100);
// 	strcpy(db, "EEEEEEEEEEABABABAEEEEEEzCDCCCCCEEEEEAEEEzxxx");

// 	std::vector<std::string> myPeptides;
// 	myPeptides.push_back("ABABABAy");
// 	myPeptides.push_back("CCCCACCy");
// 	myPeptides.push_back("DCABABAy");

// 	unsigned long matches = 0;

// 	int dmaFd = open("/dev/axi-dma1", O_RDWR);

// 	sendCommand(dmaFd, LOAD_DATABASE);
// 	sendDatabase(dmaFd, db);

// 	sendCommand(dmaFd, PROTEIN_MATCH);
// 	sendNumPept(dmaFd, myPeptides.size());

// 	for(int p=0; p<myPeptides.size(); p++){
// 		sendPeptide(dmaFd, (char *)myPeptides[p].c_str());
			
// 		matches += checkResults(dmaFd);
// 	}

// 	close(dmaFd);

// 	return matches;
// }


unsigned long KMP(std::vector<std::pair<int, std::string>> databases, std::vector<std::pair<int, std::string>> peptides, int NumDMA){

	unsigned long matches = 0;

	std::vector<int> dmas;

	assert(databases.size() % NumDMA == 0);

	openDMAs(dmas, NumDMA);

	for(int d=0; d<databases.size(); d+=NumDMA){

		for(int i=0; i<NumDMA; i++){
			sendCommand(dmas[i], LOAD_DATABASE);
			sendDatabase(dmas[i], databases[d+i]);
		}

		for(int i=0; i<NumDMA; i++){
			sendCommand(dmas[i], PROTEIN_MATCH);
			sendNumPept(dmas[i], peptides.size());
		}

		for(int p=0; p<peptides.size(); p++){
			for(int i=0; i<NumDMA; i++)
				sendPeptide(dmas[i], peptides[p]);
			

			for(int i=0; i<NumDMA; i++)
				matches += checkResultsBuffer(dmas[i]);
		}

	}

	closeDMAs(dmas);

	return matches;
}
