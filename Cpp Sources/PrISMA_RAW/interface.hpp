#ifndef HW_INTERFACE
#define HW_INTERFACE

#include <vector>
#include <iostream>

#define LOAD_DATABASE 0
#define PROTEIN_MATCH 1

unsigned long KMP(std::vector<std::pair<int, std::string>> databases, std::vector<std::pair<int, std::string>> peptides, int NumDMA);
unsigned long KMP_SW(std::vector<std::string> proteins, std::vector<std::pair<int, std::string>> peptides);
// unsigned long KMPTest();

#endif