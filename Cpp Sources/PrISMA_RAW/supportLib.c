/*
 * supportLib.c
 *
 *  Created on: Jan 24, 2015
 *      Author: gianluca
 */

#include "supportLib.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/time.h>

unsigned long getTime(){
	struct timeval tv;
	gettimeofday(&tv, NULL);
	return tv.tv_sec * 1000000 + tv.tv_usec;
}

unsigned long reconfigure(char *bitstream){
	char buf[1024];

	unsigned long start, end;

	start = getTime();
	system("echo 1 > /sys/devices/amba.0/f8007000.ps7-dev-cfg/is_partial_bitstream");

	sprintf(buf, "cat %s > /dev/xdevcfg", bitstream);
	printf("Issuing reconfiguration of %s\n", bitstream);
	system(buf);
	end = getTime();

	return end-start;
}

unsigned long readDMA_asynch(int fd, unsigned char *buffer, unsigned long length){
	unsigned long start, end;

	if(length > DMA_LEN)
		return readDMA(fd, buffer, length);

	start = getTime();

	pread(fd, buffer, length, ASYNCH_MODE);

	end = getTime();

	return end - start;
}

unsigned int readDMA_isBusy(int fd){
	char tmp[10];
	return pread(fd, tmp, 1, INQUIRE_BUSY);
}

unsigned long readDMA_transferBack(int fd, unsigned char *buffer, unsigned long length){
	unsigned long start, end;

	start = getTime();

	pread(fd, buffer, length, FINISH_TRANSFER);

	end = getTime();

	return end - start;
}

unsigned long readDMA(int fd, unsigned char *buffer, unsigned long length){
	unsigned long byteMoved = 0;
	unsigned long byteToTransfer = 0;
	unsigned long start, end;

	start = getTime();
	while(byteMoved != length){
		byteToTransfer = length - byteMoved > DMA_LEN ? DMA_LEN : length - byteMoved;
		byteMoved += read(fd, &buffer[byteMoved], byteToTransfer);
	}
	end = getTime();

	return end - start;
}

unsigned long readDMA_simple(int fd, unsigned char *buffer, unsigned long length){
	unsigned long start, end;
	
	// start = getTime();

	unsigned long dataRead = read(fd, buffer, length);

	// end = getTime();
	return dataRead;
}

unsigned long writeDMA(int fd, unsigned char *buffer, unsigned long length){
	unsigned long byteMoved = 0;
	unsigned long byteToTransfer = 0;
	unsigned long start, end;

	start = getTime();

	while(byteMoved != length){
		byteToTransfer = length - byteMoved > DMA_LEN ? DMA_LEN : length - byteMoved;
		// printf("Sending %ld : %ld/%ld\n", byteToTransfer, byteMoved, length); fflush(NULL);
		byteMoved += write(fd, &buffer[byteMoved], byteToTransfer);
	}

	// printf("DONE writing\n"); fflush(NULL);

	end = getTime();

	return end-start;
}
