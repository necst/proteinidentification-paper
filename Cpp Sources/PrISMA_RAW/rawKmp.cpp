#include <iostream>
#include <fstream>
#include <map>
#include <vector>
#include <assert.h>
#include <sys/time.h>

#include <fcntl.h>
#include <unistd.h>
#include <string.h>

#include "interface.hpp"
#include "supportLib.h"

#define DB_PREFIX "db"
#define PAD_CHAR "x"
#define PEPTIDE_LIMIT "y"
#define PROTEIN_LIMIT "z"

#define DB_MAX_SIZE 125000
#define PEPTIDE_MAX_SIZE 160


unsigned int PAD_SIZE = 0;

std::pair<int, std::string> padString(std::string str, unsigned int size){
	unsigned int len = str.size();
	unsigned int padding = len % size == 0 ? 0 : size - (len % size);

	for(int i=0; i<padding; i++){
		str += PAD_CHAR;
	}

	std::pair<int, std::string> p(len, str);

	return p;

}

void readDatabases(std::vector<std::pair<int, std::string>> & databases, int dbNum){
	for(int i=0; i<dbNum; i++){
		std::string dbName = DB_PREFIX + std::to_string(i);

		std::ifstream fileif;

		fileif.open(dbName);
		std::string content;
		fileif >> content;
		fileif.close();

		std::pair<int, std::string> pairContent = padString(content, PAD_SIZE);

		databases.push_back(pairContent);
	}
}

void readPeptides(std::vector<std::pair<int, std::string>> & peptides, char *fileName){
	std::ifstream fileif;

	fileif.open(fileName);
	std::string content;

	while(fileif >> content){
		content = content + PEPTIDE_LIMIT;
		std::pair<int, std::string> pairContent = padString(content, PAD_SIZE);
		peptides.push_back(pairContent);
	}

}


void readProteins(std::vector<std::string> & proteins, char *fileName){
	std::ifstream fileif;

	fileif.open(fileName);
	std::string content;

	while(fileif >> content){
		content = content;
		proteins.push_back(content);
	}

}



int main(int argc, char **argv){

	if(argc!=6){
		std::cout << "USAGE: exe NUMDB PeptidesFile PorteinsFile PAD_SIZE DMAS" << std::endl;
		exit(0);
	}

	unsigned long tStart, tEnd;

	int dbNum = atoi(argv[1]);
	char *peptideFile = argv[2];
	char *proteinsFile = argv[3];
	PAD_SIZE = atoi(argv[4]) / 8;
	unsigned int DMAS = atoi(argv[5]);


	std::vector<std::pair<int, std::string>> databases;
	std::vector<std::pair<int, std::string>> peptides;
	std::vector<std::string> proteins;

	readDatabases(databases, dbNum);
	readPeptides(peptides, peptideFile);
	// readProteins(proteins, proteinsFile);

	for (auto d: databases){
		assert(d.second.size() % PAD_SIZE == 0);
		assert(d.second.size() < DB_MAX_SIZE);
		// std::cout << d.second.size() << "\t" << d.first << std::endl;
	}

	for (auto p: peptides){
		assert(p.second.size() % PAD_SIZE == 0);
		assert(p.second.size() < PEPTIDE_MAX_SIZE);
		// std::cout << p.second << "\t" << p.second.size() << "\t" << p.first << std::endl;
	}

	// tStart = getTime();
	// unsigned long matches = KMPTest();
	// tEnd = getTime();

	tStart = getTime();
	unsigned long matches = KMP(databases, peptides, DMAS);
	tEnd = getTime();

	// tStart = getTime();
	// matches = KMP_SW(proteins, peptides);
	// tEnd = getTime();


	// std::cout << "Matches\t" << matches << std::endl;
	// std::cout << "Time\t" << (tEnd - tStart) << std::endl;

	std::cout << "RAW_" << DMAS << "\t" << "TutteProteineKMP" << "\t" << (tEnd - tStart) << std::endl;

}