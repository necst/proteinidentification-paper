This repository contains source codes and templates for the implementation and usage of a protein identification HW core based on the Knuth-Morris-Pratt algorithm for FPGA Devices.

The templates and projects for the Xilinx Vivado Design Suite here included targets the AVNET Zedboard development board and are tested on the device.

Contents of the repo:
	- HLS Cores: Source code for the HW cores, to be used with Vivado HLS
	- Cpp Sources: Source code for the algorithm, one for GPP processors and one for each of the HW core version
	- Vivado Templates: Templates for Xilinx Vivado 15.1
	- Zedboard SDs: Ready to boot SD files. A compiled version of PetaLinux kernel included with DMA kernel drivers is made available. Just copy the files on the board and boot it.

Some path might be hardcoded in the templates, please use them as a guideline and recreate them in your environment to be sure that no problem occurs.